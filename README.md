# Prerequisites

- docker

# How to make a single-node deployment

* Set the connection info for your SSH server as enviroment variables:
``` 
export SSH_HOST=192.168.0.69 # you can use a FQDN aswell
export SSH_USER=ubuntu
```

Note that if you are using a non standard port for SSH, or instead of using keys to login you are using password, you'll need to add `ansible_port` and `ansible_password` to the inventory file in the next step.
You can always add `ansible_port` if you want the ansible playbook to change the default port 22 to a different one (providing a bit of security by obscurity).

* Customize the yaml template below and execute this snippet from the root dir fo this git repository.
It will create your inventory file under `inventory/` dir defining only a k3s master node. You can find more customization examples (ie, cluster mode, defining some firewall rules...) under the `inventory/` dir.
```
cat > inventory/my-k8s.yaml <<EOF
master:
  hosts:
    ${SSH_HOST}:
      hostname: mydandelion
      ansible_user: ${SSH_USER}
      k3s_cluster_name: dandelion
      k3s_expose_kube_api: true
      dandelion:
        git_repo: https://gitlab.com/gimbalabs/dandelion/kustomize-dandelion.git
        git_branch: main
        networks:
          testnet:
            enabled: true
          mainnet:
            enabled: false
EOF
```

Note that this config will open the port to connect to the k8s api (`k3s_expose_kube_api: true`), and you don't want to do that for a server exposed to the Internet. By default, only SSH, HTTP and HTTPS ports will be opened.

* Ensure you are either not using ssh keys to connect to remote host (using `ansible_password`) or you have ssh keypair created with:
```
ssh-keygen
```

* Run ansible from the root of this git repository using the previously created inventory file:

``` 
INVENTORY_FILE=inventory/myhost.yaml

docker run --rm -it \
       -v $HOME/.kube:/root/.kube \
       -v $HOME/.local/bin:/root/.local/bin \
       -v $HOME/.ssh/id_rsa:/root/.ssh/id_rsa:Z,ro \
       -v $PWD/.ansible:/root/.ansible \
       -v $PWD:/ansible \
       --entrypoint bash \
        gimbalabs/ansible:latest \
       -c "ansible-galaxy install -r requirements.yaml && ansible-playbook -i ${INVENTORY_FILE} dandelion-node.yaml"
```
* A `kubeconfig` file with the name of the cluster name will be placed at your home directory, but you need to ensure permissions correct for your user (ansible runs as root within the container):
```
chown -R ${USER}: ~/.kube
```
* Check your dandelion deploy is progressing using `kubectl`:
```
export CLUSTER_NAME=dandelion
export KUBECONFIG=$HOME/${CLUSTER_NAME}.yaml
kubectl get pods -A
```

For more information of dandelion hardware requirements and operations visit the main [kustomize-dandelion] repository.

[kustomize-dandelion]: https://gitlab.com/gimbalabs/dandelion/kustomize-dandelion
